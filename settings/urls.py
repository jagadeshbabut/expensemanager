from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from djrill import DjrillAdminSite

from expense.views import expense, expense_delete, expense_edit
from income.views import income, income_delete, income_edit
from openidapp.views import dashboard, social_sharing, balance_info
from reports.views import reports
from userinfo.views import user_info, user_info_edit


admin.site = DjrillAdminSite()
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^$', dashboard, name='dashboard'),
    url('^accounts/', include('django.contrib.auth.urls')),
    url(r'^income/', income, name='income'),
    url(r'^income_edit/(?P<income_id>\d+)/$', income_edit, name='income_edit'),
    url(r'^income_delete/(?P<income_id>\d+)/$', income_delete, name='income_delete'),
    url(r'^expense/', expense, name='expense'),
    url(r'^expense_edit/(?P<expense_id>\d+)/$', expense_edit, name='expense_edit'),
    url(r'^expense_delete/(?P<expense_id>\d+)/$', expense_delete, name='expense_delete'),
    url(r'^social_sharing/', social_sharing, name='social_sharing'),
    url(r'^user_info/', user_info, name='user_info'),
    url(r'^user_info_edit/', user_info_edit, name='user_info_edit'),
    url(r'^balance_info/', balance_info, name='balance_info'),
    url(r'^reports/', reports, name='reports'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))