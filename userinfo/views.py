from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from userinfo.forms import UserInfoForm
from userinfo.models import UserInfo


@login_required
def user_info(request):
    user_info =  UserInfo.objects.filter(user = request.user)
    return render(request, 'user_info.html', {'forms' : user_info})

@login_required
@require_http_methods(["GET", "POST"])
def user_info_edit(request):
    if request.method == 'GET':
        obj =  UserInfo.objects.filter(user = request.user)
        if not obj:
            form = UserInfoForm()
            return render(request, 'user_info_edit.html', {'form' : form})
        obj  = UserInfo.objects.get(user = request.user)
        form = UserInfoForm(initial = {
                                        'first_name' :   obj.first_name,
                                        'last_name'  :   obj.last_name,
                                        'mobile'     :   obj.mobile,
                                        'occupation' :   obj.occupation,
                                        'website_url':   obj.website_url,
                                        'interests'  :   obj.interests,
                                        'about'      :   obj.about,
                                        'image'      :   obj.img,
                                        })
        return render(request, 'user_info_edit.html',{'form' : form} )
    else:
        form = UserInfoForm(request.POST, request.FILES)
        if form.is_valid():
            obj         =   UserInfo.objects.filter(user = request.user)
            first_name  =   form.cleaned_data['first_name']
            last_name   =   form.cleaned_data['last_name']
            mobile      =   form.cleaned_data['mobile']
            occupation  =   form.cleaned_data['occupation']
            website_url =   form.cleaned_data['website_url']
            interests   =   form.cleaned_data['interests']
            about       =   form.cleaned_data['about']
            image       =   form.cleaned_data['image']
            if obj:
                obj.user        =   request.user
                obj.first_name  =   form.cleaned_data['first_name']
                obj.last_name   =   form.cleaned_data['last_name']
                obj.mobile      =   form.cleaned_data['mobile']
                obj.occupation  =   form.cleaned_data['occupation']
                obj.website_url =   form.cleaned_data['website_url']
                obj.interests   =   form.cleaned_data['interests']
                obj.about       =   form.cleaned_data['about']
                obj.image       =   form.cleaned_data['image']
                obj.update()
                return HttpResponseRedirect('/user_info/')
            else:
                UserInfo.objects.create(user        = request.user,
                                        first_name  = first_name,
                                        last_name   = last_name,
                                        mobile      = mobile,
                                        occupation  = occupation,
                                        website_url = website_url,
                                        interests   = interests,
                                        about       = about,
                                        image       = image)
        return HttpResponseRedirect('/user_info/')