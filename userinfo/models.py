from django.contrib.auth.models import User
from django.db import models


class UserInfo(models.Model):
    first_name  =   models.CharField(max_length = 50)
    last_name   =   models.CharField(max_length = 50)
    mobile      =   models.CharField(max_length = 12)
    occupation  =   models.CharField(max_length = 50, blank = True)
    website_url =   models.URLField(blank = True)
    interests   =   models.CharField(max_length = 100, blank = True)
    about       =   models.TextField(max_length = 2000)
    user        =   models.ForeignKey(User)
    img         =   models.ImageField(upload_to = "media", null = True, blank = True, editable = True)

    def __unicode__(self):
        return self.first_name
