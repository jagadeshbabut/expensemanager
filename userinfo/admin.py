from django.contrib import admin
from userinfo.models import UserInfo


class UserInfoAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'mobile', 'occupation',
                    'website_url', 'interests', 'about', 'img']

admin.site.register(UserInfo, UserInfoAdmin)