from django import forms
from django.forms.widgets import PasswordInput

class UserInfoForm(forms.Form):
    first_name  =   forms.CharField(max_length = 50)
    last_name   =   forms.CharField(max_length = 50)
    mobile      =   forms.CharField(max_length = 12)
    occupation  =   forms.CharField(max_length = 50,    required = False)
    website_url =   forms.URLField(initial = 'http://', required = False)
    interests   =   forms.CharField(max_length = 100,   required = False)
    about       =   forms.CharField(widget = forms.Textarea)
    img         =   forms.ImageField()

class UserPasswordForm(forms.Form):
    pswd        =   forms.CharField(widget = PasswordInput())
