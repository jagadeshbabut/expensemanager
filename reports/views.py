from django.shortcuts import render
from expense.models import Expense, ExpenseType
from income.models import Income, IncomeType
from openidapp.views import both_balance_and_expense, listing, only_income, only_expense


def reports(request):
    inc_keys    =   Income.objects.values_list('type', flat=True)
    exp_keys    =   Expense.objects.values_list('type', flat=True)
    inc_data,inc_label,exp_label,exp_data = [], [], [], []

    for i in set(inc_keys):
        income  = Income.objects.filter(delete_flag=False, type=i).values_list('amount', flat=True)
        inc= IncomeType.objects.get(id = i)
        inc_label.append(str(inc.name))
        inc_data.append(sum(income))

    for i in set(exp_keys):
        expense = Expense.objects.filter(delete_flag=False, type=i).values_list('amount', flat=True)
        exp     = ExpenseType.objects.get(id = i)
        exp_label.append(str(exp.name))
        exp_data.append(sum(expense))

    user_inc = Income.objects.filter(user = request.user, delete_flag = False).order_by('-date')
    income  = Income.objects.filter(user = request.user, delete_flag = False)
    expense = Expense.objects.filter(user = request.user, delete_flag = False)
    if income and expense:
        bal_info = both_balance_and_expense(income, expense)
        pagination      = listing(request, user_inc)
        return render(request, 'reports.html', {'inc_amount': inc_data,
                                                'inc_type': inc_label,
                                                'exp_amount': exp_data,
                                                'exp_type': exp_label,
                                                'listing'    : pagination,
                                               'bal_info'   : bal_info,})
    elif income:
        bal_info = only_income(income)
        pagination      = listing(request, user_inc)
        return render(request, 'reports.html', {'inc_amount': inc_data,
                                                'inc_type': inc_label,
                                                'exp_amount': exp_data,
                                                'exp_type': exp_label,
                                                'listing'    : pagination,
                                               'bal_info'   : bal_info,})
    elif expense:
        bal_info = only_expense(expense)
        pagination      = listing(request,user_inc)
        return render(request, 'reports.html', {'inc_amount': inc_data,
                                                'inc_type': inc_label,
                                                'exp_amount': exp_data,
                                                'exp_type': exp_label,
                                                'listing'    : pagination,
                                               'bal_info'   : bal_info,})
    else:
        bal_info = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
        return render(request, 'reports.html', {'inc_amount': inc_data,
                                                'inc_type': inc_label,
                                                'exp_amount': exp_data,
                                                'exp_type': exp_label,
                                                'bal_info'   : bal_info,})

# def reports(request):
#     keys    =   Income.objects.values_list('amount', flat=True)
#     values  =   Income.objects.values_list('date', flat=True)
#
#     data_year   =   [i.year for i in values]
#     data_month   =   [i.strftime("%B") for i in values]
#     data_date   =   [i.day for i in values]
#     data    =   [[keys[i],values[i]] for i in range(len(keys))]
#
#     return render(request, 'reports.html', {'data':data})
#
#     values      =   Income.objects.values_list('date', flat=True)
#     data_year   =   [i.year for i in values]
#     data_month  =   [i.strftime('%B') for i in values]
#     data_date   =   [i.day for i in values]
#     for i in set(data_year):
#         income    =    Income.objects.filter(date__year = i)
#         income_type     =    Income.objects.filter(date__year = i)
#         total_income    = income.aggregate(Sum('amount'))
#         total_income    = int(total_income.get('amount__sum'))
#         data.append([i,total_income])
#     for i in set(inc_keys):
#         income  = Income.objects.filter(delete_flag=False, type=i).values_list('amount', flat=True)
#         inc_label.append(i)
#         inc_data.append(sum(income))
#
#     return render(request, 'reports.html', {'inc_amount': inc_data,
#                                             'inc_type': inc_label,
#                                             'exp_amount': exp_data,
#                                             'exp_type': exp_label })