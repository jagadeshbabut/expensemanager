from django.contrib.auth.models import User
from django.db import models


class ExpenseType(models.Model):
    name        = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.name

class Expense(models.Model):
    user        = models.ForeignKey(User)
    type        = models.ForeignKey(ExpenseType)
    delete_flag = models.BooleanField(default = False)
    exp_flag    = models.BooleanField(default = True)
    name        = models.CharField(max_length = 200)
    ref_no      = models.CharField(max_length = 100)
    amount      = models.PositiveIntegerField()
    date        = models.DateField()

    def __unicode__(self):
        return self.name
