import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from expense.forms import ExpenseForm, ExpenseDeleteForm, ExpenseDeleteForm2
from expense.models import Expense
from income.models import Income
from openidapp.views import both_balance_and_expense, \
    only_income, only_expense
from openidapp.views import listing
from utils.app_messages import *
from utils.app_messages import ENTRY_ADD_SUCCESSFUL, \
    ENTRY_UPDATE_SUCCESSFUL, ENTRY_DELETE_SUCCESSFUL


@login_required
@require_http_methods(["GET", "POST"])
def expense(request):
    form = ExpenseForm()
    if request.method == 'GET':
        del_form     = ExpenseDeleteForm()
        user_exp     = Expense.objects.filter(user = request.user, delete_flag = False).order_by('-date')
        income  = Income.objects.filter(user = request.user, delete_flag = False)
        expense = Expense.objects.filter(user = request.user, delete_flag = False)
        if income and expense:
            bal_info = both_balance_and_expense(income, expense)
            pagination      = listing(request, user_exp)
            return render(request, 'expense.html', {'form'      : form,
                                                   'listing'    : pagination,
                                                   'del_form'   : del_form,
                                                   'bal_info'   : bal_info,})
        elif income:
            bal_info = only_income(income)
            pagination      = listing(request, user_exp)
            return render(request, 'expense.html', {'form'      : form,
                                                   'listing'    : pagination,
                                                   'del_form'   : del_form,
                                                   'bal_info'   : bal_info,})
        elif expense:
            bal_info = only_expense(expense)
            pagination      = listing(request,user_exp)
            return render(request, 'expense.html', {'form'      : form,
                                                   'listing'    : pagination,
                                                   'del_form'   : del_form,
                                                   'bal_info'   : bal_info,})
        else:
            bal_info = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return render(request, 'expense.html', {'form'      : form,
                                                   'del_form'   : del_form,
                                                   'bal_info'   : bal_info,})

    else:
        form = ExpenseForm(request.POST)
        if not form.is_valid():
            user_exp    = Expense.objects.filter(user = request.user, delete_flag = False).order_by('-date')
            pagination  = listing(request, user_exp)
            return render(request, 'expense.html', {'form':form, 'listing':pagination})
        name         = form.cleaned_data['name']
        ref_no       = form.cleaned_data['ref_no']
        date         = form.cleaned_data['date']
        expense_type = form.cleaned_data['type']
        amount       = form.cleaned_data['amount']
        Expense.objects.create(name   = name,
                               ref_no = ref_no,
                               date   = date,
                               type   = expense_type,
                               amount = amount,
                               user   = request.user)
        messages.success(request, ENTRY_ADD_SUCCESSFUL, fail_silently = True)
        return HttpResponseRedirect('/expense/')

@login_required
@require_http_methods(["GET", "POST"])
def expense_edit(request, expense_id):
    form = ExpenseForm()
    if request.method == 'GET':
        instance    = Expense.objects.get(user = request.user, delete_flag = False, pk = expense_id)
        return HttpResponse(json.dumps({'pk'      : instance.pk,
                                        'name'    : instance.name,
                                        'ref_no'  : instance.ref_no,
                                        'date'    : instance.date.isoformat(),
                                        'type'    : instance.type.name,
                                        'amount'  : instance.amount}), content_type='application/json')
    else:
        form = ExpenseForm(request.POST)
        edit_details = Expense.objects.get(user = request.user, delete_flag = False, pk = expense_id)
        if not form.is_valid():
            user_inc_edit     = Expense.objects.get(user = request.user, delete_flag = False, pk = expense_id)
            return render(request, 'expense_edit.html', {'form':form, 'obj':user_inc_edit})
        edit_details.name        = form.cleaned_data['name']
        edit_details.ref_no      = form.cleaned_data['ref_no']
        edit_details.date        = form.cleaned_data['date']
        edit_details.type        = form.cleaned_data['type']
        edit_details.amount      = form.cleaned_data['amount']
        edit_details.save()
        edit_details = Expense.objects.get(user = request.user, delete_flag = False, pk = expense_id)
        messages.success(request, ENTRY_UPDATE_SUCCESSFUL, fail_silently = True)
        return HttpResponse(json.dumps({'pk'      : edit_details.pk,
                                        'name'    : edit_details.name,
                                        'ref_no'  : edit_details.ref_no,
                                        'date'    : edit_details.date.isoformat(),
                                        'type'    : edit_details.type.name,
                                        'amount'  : edit_details.amount}), content_type='application/json')

@login_required
@require_http_methods(["POST"])
def expense_delete(request, expense_id):
    if request.method == 'POST':
        form         = ExpenseDeleteForm2(request.POST)
        val          = Expense.objects.filter(pk = expense_id)
        if not val:
            return HttpResponse("Sorry, The Entry has already been removed!")
        instance             = Expense.objects.get(pk = expense_id)
        instance.delete_flag = True
        instance.save()
        messages.success(request, ENTRY_DELETE_SUCCESSFUL, fail_silently = True)
    return HttpResponseRedirect('/expense/')