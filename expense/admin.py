from django.contrib import admin
from expense.models import Expense, ExpenseType


class ExpenseAdmin(admin.ModelAdmin):
    list_display = ('user', 'ref_no', 'date', 'type', 'amount')

class ExpenseTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )

admin.site.register(Expense,ExpenseAdmin)
admin.site.register(ExpenseType,ExpenseTypeAdmin)