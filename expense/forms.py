from django import forms
from expense.models import ExpenseType


class ExpenseForm(forms.Form):
    name        = forms.CharField(max_length = 100)
    ref_no      = forms.CharField(max_length = 100)
    amount      = forms.IntegerField()
    date        = forms.DateField()
    type        = forms.ModelChoiceField(queryset = ExpenseType.objects.all(), to_field_name = 'name')

class ExpenseDeleteForm(forms.Form):
    income_id   = forms.IntegerField()

class ExpenseDeleteForm2(forms.Form):
    pass