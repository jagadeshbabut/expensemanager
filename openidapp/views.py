import datetime
import json
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models.aggregates import Sum
from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from expense.models import Expense
from income.models import Income


def only_income(income):
    total_income    = income.aggregate(Sum('amount'))
    total_income    = int(total_income.get('amount__sum'))
    bal_info = {'total_income'  : total_income,
                'total_expense' : 0,
                'total_balance' : total_income,
                'neg_bal_flag'  : False}
    return bal_info

def only_expense(expense):
    total_expense    = expense.aggregate(Sum('amount'))
    total_expense    = int(total_expense.get('amount__sum'))
    bal_info = {'total_income'  : 0,
                'total_expense' : total_expense,
                'total_balance' : total_expense,
                'neg_bal_flag'  : True}
    return bal_info

def both_balance_and_expense(income, expense):
    total_income    = income.aggregate(Sum('amount'))
    total_income    = int(total_income.get('amount__sum'))
    total_expense   = expense.aggregate(Sum('amount'))
    total_expense   = int(total_expense.get('amount__sum'))
    total_balance   = total_income - total_expense
    if total_balance < 0:
        neg_bal_flag    = True
        total_balance  *= -1
        bal_info = {'total_income'  : total_income,
                    'total_expense' : total_expense,
                    'total_balance' : total_balance,
                    'neg_bal_flag'  : neg_bal_flag}
        return bal_info
    else:
        neg_bal_flag = False
        bal_info = {'total_income'  : total_income,
                    'total_expense' : total_expense,
                    'total_balance' : total_balance,
                    'neg_bal_flag'  : neg_bal_flag}
        return bal_info

@login_required
def balance_info(request):
    if request.GET.get('value') == 'today':
        date = datetime.date.today()
        income  = Income.objects.filter(user = request.user, delete_flag = False, date = date)
        expense = Expense.objects.filter(user = request.user, delete_flag = False, date = date)
        if income and expense:
            bal_dict = both_balance_and_expense(income, expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif income:
            bal_dict = only_income(income)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif expense:
            bal_dict = only_expense(expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        else:
            bal_dict = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
    elif request.GET.get('value') == 'yesterday':
        startdate = datetime.date.today() - datetime.timedelta(days=1)
        enddate = datetime.date.today() - datetime.timedelta(days=1)
        income  = Income.objects.filter(user = request.user, delete_flag = False, date__range=[startdate, enddate])
        expense = Expense.objects.filter(user = request.user, delete_flag = False, date__range=[startdate, enddate])
        if income and expense:
            bal_dict = both_balance_and_expense(income, expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif income:
            bal_dict = only_income(income)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif expense:
            bal_dict = only_expense(expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        else:
            bal_dict = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
    elif request.GET.get('value') == 'last_week':
        startdate = datetime.date.today() - datetime.timedelta(days=7)
        enddate = datetime.date.today()
        income  = Income.objects.filter(user = request.user, delete_flag = False, date__range=[startdate, enddate])
        expense = Expense.objects.filter(user = request.user, delete_flag = False, date__range=[startdate, enddate])
        if income and expense:
            bal_dict = both_balance_and_expense(income, expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif income:
            bal_dict = only_income(income)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif expense:
            bal_dict = only_expense(expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        else:
            bal_dict = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
    elif request.GET.get('value') == 'last_month':
        startdate = datetime.date.today() - datetime.timedelta(days=30)
        enddate = datetime.date.today()
        income  = Income.objects.filter(user = request.user, delete_flag = False, date__range=[startdate, enddate])
        expense = Expense.objects.filter(user = request.user, delete_flag = False, date__range=[startdate, enddate])
        if income and expense:
            bal_dict = both_balance_and_expense(income, expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif income:
            bal_dict = only_income(income)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif expense:
            bal_dict = only_expense(expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        else:
            bal_dict = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
    else:
        income  = Income.objects.filter(user = request.user, delete_flag = False)
        expense = Expense.objects.filter(user = request.user, delete_flag = False)
        if income and expense:
            bal_dict = both_balance_and_expense(income, expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif income:
            bal_dict = only_income(income)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        elif expense:
            bal_dict = only_expense(expense)
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')
        else:
            bal_dict = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return HttpResponse(json.dumps(bal_dict), content_type='application/json')

    income  = Income.objects.filter(user = request.user, delete_flag = False)
    expense = Expense.objects.filter(user = request.user, delete_flag = False)
    total_income    = income.aggregate(Sum('amount'))
    total_income    = int(total_income.get('amount__sum'))
    total_expense   = expense.aggregate(Sum('amount'))
    total_expense   = int(total_expense.get('amount__sum'))
    total_balance   = total_income - total_expense
    if total_balance < 0:
        neg_bal_flag = True
        total_balance *= -1
        bal_info = {'total_income'  : total_income,
                    'total_expense' : total_expense,
                    'total_balance' : total_balance,
                    'neg_bal_flag'  : neg_bal_flag}
        return bal_info
    else:
        neg_bal_flag = False
        bal_info = {'total_income'  : total_income,
                    'total_expense' : total_expense,
                    'total_balance' : total_balance,
                    'neg_bal_flag'  : neg_bal_flag}
        return bal_info

@login_required
@require_http_methods(["GET","POST"])
def dashboard(request):
    authen      = request.user.has_usable_password()
    if request.method == "GET" and request.GET.get('category') == "credit":
        incomes = Income.objects.filter(user = request.user, delete_flag = False).order_by('-date')
        income  = Income.objects.filter(user = request.user, delete_flag = False)
        expense = Expense.objects.filter(user = request.user, delete_flag = False)
        if income and expense:
            bal_info = both_balance_and_expense(income, expense)
            pagination      = listing(request, incomes)
            return render(request, 'dashboard.html', {'authen'  : authen,
                                                      'listing' : pagination,
                                                      'bal_info': bal_info,
                                                      'credit_value' : True,})

        elif income:
            bal_info = only_income(income)
            pagination      = listing(request, incomes)
            return render(request, 'dashboard.html', {'authen'  : authen,
                                                      'listing' : pagination,
                                                      'bal_info': bal_info,
                                                      'credit_value' : True,})

        elif expense:
            bal_info = (expense)
            pagination      = listing(request, incomes)
            return render(request, 'dashboard.html', {'authen'  : authen,
                                                      'listing' : pagination,
                                                      'bal_info': bal_info,
                                                      'credit_value' : True,})
        else:
            bal_info = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return render(request, 'dashboard.html', {'authen'  : authen,
                                                      'bal_info': bal_info,
                                                      'credit_value' : True,})

#     if request.method == "GET" and request.GET.get('category') == "credit":
#         incomes = Income.objects.filter(user = request.user, delete_flag = False).order_by('-date')
#         income  = Income.objects.filter(user = request.user, delete_flag = False)
#         expense = Expense.objects.filter(user = request.user, delete_flag = False)
#         total_income    = income.aggregate(Sum('amount'))
#         total_income    = int(total_income.get('amount__sum'))
#         total_expense   = expense.aggregate(Sum('amount'))
#         total_expense   = int(total_expense.get('amount__sum'))
#         total_balance   = total_income - total_expense
#         pagination      = listing(request, incomes)
#         if total_balance < 0:
#             neg_bal_flag = True
#             total_balance *= -1
#             bal_info = {'total_income'  : total_income,
#                         'total_expense' : total_expense,
#                         'total_balance' : total_balance,
#                         'neg_bal_flag'  : neg_bal_flag}
#             return render(request, 'dashboard.html', {'authen'       : authen,
#                                                       'bal_info'     : bal_info,
#                                                       'listing'        : pagination,
#                                                       'credit_value' : True})
#         else:
#             neg_bal_flag = False
#             bal_info = {'total_income'  : total_income,
#                         'total_expense' : total_expense,
#                         'total_balance' : total_balance,
#                         'neg_bal_flag'  : neg_bal_flag}
#             return render(request, 'dashboard.html',{'authen'       : authen,
#                                                      'bal_info'     : bal_info,
#                                                      'listing'        : pagination,
#                                                      'credit_value' : True})

    elif request.method == "GET" and request.GET.get('category') == "debit":
        expenses = Expense.objects.filter(user = request.user, delete_flag = False).order_by('-date')
        income  = Income.objects.filter(user = request.user, delete_flag = False)
        expense = Expense.objects.filter(user = request.user, delete_flag = False)
        if income and expense:
            bal_info = both_balance_and_expense(income, expense)
            pagination      = listing(request, expenses)
            return render(request, 'dashboard.html', {'authen'  : authen,
                                                      'listing' : pagination,
                                                      'bal_info': bal_info,
                                                      'debit_value' : True,})

        elif income:
            bal_info = only_income(income)
            pagination      = listing(request, expenses)
            return render(request, 'dashboard.html', {'authen'  : authen,
                                                      'listing' : pagination,
                                                      'bal_info': bal_info,
                                                      'debit_value' : True,})

        elif expense:
            bal_info = (expense)
            pagination      = listing(request, expenses)
            return render(request, 'dashboard.html', {'authen'  : authen,
                                                      'listing' : pagination,
                                                      'bal_info': bal_info,
                                                      'debit_value' : True,})
        else:
            bal_info = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return render(request, 'dashboard.html', {'authen'  : authen,
                                                      'bal_info': bal_info,
                                                      'debit_value' : True,})

#         total_income    = income.aggregate(Sum('amount'))
#         total_income    = int(total_income.get('amount__sum'))
#         total_expense   = expense.aggregate(Sum('amount'))
#         total_expense   = int(total_expense.get('amount__sum'))
#         total_balance   = total_income - total_expense
#         pagination      = listing(request, expenses)
#         if total_balance < 0:
#             neg_bal_flag = True
#             total_balance *= -1
#             bal_info = {'total_income'  : total_income,
#                         'total_expense' : total_expense,
#                         'total_balance' : total_balance,
#                         'neg_bal_flag'  : neg_bal_flag}
#             return render(request, 'dashboard.html', {'authen'       : authen,
#                                                       'bal_info'     : bal_info,
#                                                       'listing'        : pagination,
#                                                       'debit_value'  : True})
#         else:
#             neg_bal_flag = False
#             bal_info = {'total_income'  : total_income,
#                         'total_expense' : total_expense,
#                         'total_balance' : total_balance,
#                         'neg_bal_flag'  : neg_bal_flag}
#             return render(request, 'dashboard.html',{'authen'       : authen,
#                                                      'bal_info'     : bal_info,
#                                                      'listing'        : pagination,
#                                                      'debit_value'  : True})

    income  = Income.objects.filter(user = request.user, delete_flag = False)
    expense = Expense.objects.filter(user = request.user, delete_flag = False)
    if income and expense:
        bal_info = both_balance_and_expense(income, expense)
        all_records = list(income) + list(expense)
        all_records.sort(key=lambda item: item.date, reverse = True)
        pagination      = listing(request, all_records)
        return render(request, 'dashboard.html', {'authen'  : authen,
                                                  'listing' : pagination,
                                                  'bal_info': bal_info})

    elif income:
        bal_info = only_income(income)
        all_records = list(income)
        all_records.sort(key=lambda item: item.date, reverse = True)
        pagination      = listing(request, all_records)
        return render(request, 'dashboard.html', {'authen'  : authen,
                                                  'listing' : pagination,
                                                  'bal_info': bal_info})

    elif expense:
        bal_info = only_expense(expense)
        all_records = list(expense)
        all_records.sort(key=lambda item: item.date, reverse = True)
        pagination      = listing(request, all_records)
        return render(request, 'dashboard.html', {'authen'  : authen,
                                                  'listing' : pagination,
                                                  'bal_info': bal_info})
    else:
        bal_info = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
        return render(request, 'dashboard.html', {'authen'  : authen,
                                                  'bal_info': bal_info})

@login_required
def social_sharing(request):
    send_mail("It works!",
              "This will get sent through Mandrill",
              "Jagadesh Babu T <jaga@jagadesh.in>",
              ["jagadesh12game@gmail.com"])
    return render(request, 'social_sharing.html')


def listing(request, obj_list):
    paginator   = Paginator(obj_list, 10)
    page        = request.GET.get('page')
    try:
        listings = paginator.page(page)
    except PageNotAnInteger:
        listings = paginator.page(1)
    except EmptyPage:
        listings = paginator.page(paginator.num_pages)
    return listings