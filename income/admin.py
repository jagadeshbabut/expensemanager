from django.contrib import admin
from income.models import Income, IncomeType


class IncomeAdmin(admin.ModelAdmin):
    list_display = ('user', 'ref_no', 'date', 'type', 'amount')

class IncomeTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )

admin.site.register(Income,IncomeAdmin)
admin.site.register(IncomeType,IncomeTypeAdmin)