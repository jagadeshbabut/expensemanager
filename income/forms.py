from django import forms
from income.models import IncomeType


class IncomeForm(forms.Form):
    name        = forms.CharField(max_length = 100)
    ref_no      = forms.CharField(max_length = 100)
    amount      = forms.IntegerField()
    date        = forms.DateField()
    type        = forms.ModelChoiceField(queryset = IncomeType.objects.all(),to_field_name = 'name')

class IncomeDeleteForm(forms.Form):
    income_id   = forms.IntegerField()

class IncomeDeleteForm2(forms.Form):
    pass
