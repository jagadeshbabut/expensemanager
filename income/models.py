from django.contrib.auth.models import User
from django.db import models


class IncomeType(models.Model):
    name        = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.name

class Income(models.Model):
    user        = models.ForeignKey(User)
    type        = models.ForeignKey(IncomeType)
    delete_flag = models.BooleanField(default = False)
    inc_flag    = models.BooleanField(default = True)
    name        = models.CharField(max_length = 200)
    ref_no      = models.CharField(max_length = 100)
    amount      = models.IntegerField()
    date        = models.DateField()

    def __unicode__(self):
        return self.name
