import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from expense.models import Expense
from income.forms import IncomeForm, IncomeDeleteForm, IncomeDeleteForm2
from income.models import Income
from openidapp.views import listing, both_balance_and_expense, only_income, \
    only_expense
from utils.app_messages import ENTRY_ADD_SUCCESSFUL, ENTRY_UPDATE_SUCCESSFUL, \
    ENTRY_DELETE_SUCCESSFUL


@login_required
@require_http_methods(["GET", "POST"])
def income(request):
    form = IncomeForm()
    if request.method == 'GET':
        del_form = IncomeDeleteForm()
        user_inc = Income.objects.filter(user = request.user, delete_flag = False).order_by('-date')
        income  = Income.objects.filter(user = request.user, delete_flag = False)
        expense = Expense.objects.filter(user = request.user, delete_flag = False)
        if income and expense:
            bal_info = both_balance_and_expense(income, expense)
            pagination      = listing(request, user_inc)
            return render(request, 'income.html', {'form'      : form,
                                                   'listing'    : pagination,
                                                   'del_form'   : del_form,
                                                   'bal_info'   : bal_info,})
        elif income:
            bal_info = only_income(income)
            pagination      = listing(request, user_inc)
            return render(request, 'income.html', {'form'      : form,
                                                   'listing'    : pagination,
                                                   'del_form'   : del_form,
                                                   'bal_info'   : bal_info,})
        elif expense:
            bal_info = only_expense(expense)
            pagination      = listing(request,user_inc)
            return render(request, 'income.html', {'form'      : form,
                                                   'listing'    : pagination,
                                                   'del_form'   : del_form,
                                                   'bal_info'   : bal_info,})
        else:
            bal_info = {'total_income' : 0, 'total_expense' : 0 ,'total_balance' : 0}
            return render(request, 'income.html', {'form'      : form,
                                                   'del_form'   : del_form,
                                                   'bal_info'   : bal_info,})

    else:
        form = IncomeForm(request.POST)
        if not form.is_valid():
            user_inc     = Income.objects.filter(user = request.user, delete_flag = False).order_by('-date')
            pagination      = listing(request, user_inc)
            return render(request, 'income.html', {'form':form, 'obj':pagination})
        name        = form.cleaned_data['name']
        ref_no      = form.cleaned_data['ref_no']
        date        = form.cleaned_data['date']
        income_type = form.cleaned_data['type']
        amount      = form.cleaned_data['amount']
        Income.objects.create(name    = name,
                              ref_no  = ref_no,
                              date    = date,
                              type    = income_type,
                              amount  = amount,
                              user    = request.user)
        messages.success(request, ENTRY_ADD_SUCCESSFUL, fail_silently = True)
        return HttpResponseRedirect('/income/')

@login_required
@require_http_methods(["GET", "POST"])
def income_edit(request, income_id):
    form = IncomeForm()
    if request.method == 'GET':
        instance    = Income.objects.get(user = request.user, delete_flag = False, pk = income_id)
        return HttpResponse(json.dumps({'pk'      : instance.pk,
                                        'name'    : instance.name,
                                        'ref_no'  : instance.ref_no,
                                        'date'    : instance.date.isoformat(),
                                        'type'    : instance.type.name,
                                        'amount'  : instance.amount}), content_type='application/json')
    else:
        form = IncomeForm(request.POST)
        edit_details = Income.objects.get(user = request.user, delete_flag = False, pk = income_id)
        if not form.is_valid():
            user_inc_edit     = Income.objects.get(user = request.user, delete_flag = False, pk = income_id)
            return render(request, 'income_edit.html', {'form':form, 'obj':user_inc_edit})
        edit_details.name        = form.cleaned_data['name']
        edit_details.ref_no      = form.cleaned_data['ref_no']
        edit_details.date        = form.cleaned_data['date']
        edit_details.type        = form.cleaned_data['type']
        edit_details.amount      = form.cleaned_data['amount']
        edit_details.save()
        edit_details = Income.objects.get(user = request.user, delete_flag = False, pk = income_id)
        messages.success(request, ENTRY_UPDATE_SUCCESSFUL, fail_silently = True)
        return HttpResponse(json.dumps({'pk'      : edit_details.pk,
                                        'name'    : edit_details.name,
                                        'ref_no'  : edit_details.ref_no,
                                        'date'    : edit_details.date.isoformat(),
                                        'type'    : edit_details.type.name,
                                        'amount'  : edit_details.amount}), content_type='application/json')

@login_required
@require_http_methods(["POST"])
def income_delete(request, income_id):
    if request.method == 'POST':
        form        = IncomeDeleteForm2(request.POST)
        val         = Income.objects.filter(pk= income_id)
        if not val:
            return HttpResponse("Sorry, The data has already been removed!")
        instance = Income.objects.get(pk = income_id)
        instance.delete_flag = True
        instance.save()
        messages.success(request, ENTRY_DELETE_SUCCESSFUL, fail_silently = True)
    return HttpResponseRedirect('/income/')

